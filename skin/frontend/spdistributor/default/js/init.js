// page init
jQuery(function(){
	HomeBanner();
	FeaturedProducts();
});

// homepage banner
function HomeBanner() {
	$(".home-banner").owlCarousel({
		  navigation : false,
		  pagination : true,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  singleItem : true
      });
}

// featured products
function FeaturedProducts() {
	$(".featured-slider").owlCarousel({
		  navigation : true,
		  pagination : false,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  items : 3,
		  itemsDesktop : [1199,3],
		  itemsDesktopSmall : [979,3],
		  itemsTablet : [768,2],
		  itemsMobile :	[479,1]
      });
}