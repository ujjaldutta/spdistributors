<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Uipl_Expirycategory_Adminhtml_AddexpirydayController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("expirycategory/addexpiryday")->_addBreadcrumb(Mage::helper("adminhtml")->__("Addexpiryday  Manager"),Mage::helper("adminhtml")->__("Addexpiryday Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Expirycategory"));
			    $this->_title($this->__("Manager Addexpiryday"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Expirycategory"));
				$this->_title($this->__("Addexpiryday"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("expirycategory/addexpiryday")->load($id);
				if ($model->getId()) {
					Mage::register("addexpiryday_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("expirycategory/addexpiryday");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addexpiryday Manager"), Mage::helper("adminhtml")->__("Addexpiryday Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addexpiryday Description"), Mage::helper("adminhtml")->__("Addexpiryday Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("expirycategory/adminhtml_addexpiryday_edit"))->_addLeft($this->getLayout()->createBlock("expirycategory/adminhtml_addexpiryday_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("expirycategory")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Expirycategory"));
		$this->_title($this->__("Addexpiryday"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("expirycategory/addexpiryday")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("addexpiryday_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("expirycategory/addexpiryday");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addexpiryday Manager"), Mage::helper("adminhtml")->__("Addexpiryday Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addexpiryday Description"), Mage::helper("adminhtml")->__("Addexpiryday Description"));


		$this->_addContent($this->getLayout()->createBlock("expirycategory/adminhtml_addexpiryday_edit"))->_addLeft($this->getLayout()->createBlock("expirycategory/adminhtml_addexpiryday_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("expirycategory/addexpiryday")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Addexpiryday was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setAddexpirydayData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setAddexpirydayData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("expirycategory/addexpiryday");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("expirycategory/addexpiryday");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
}
