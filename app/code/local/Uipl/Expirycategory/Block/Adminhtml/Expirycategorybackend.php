<?php  

class Uipl_Expirycategory_Block_Adminhtml_Expirycategorybackend extends Mage_Adminhtml_Block_Template
{
    protected $cron = null;

    protected function _construct() {
        parent::_construct();

        $this->cron = Mage::getSingleton('expirycategory/cron');
    }
    public function expiry_day()
    {
        return $this->cron->changeCategory();
    }
    function getAttributeOptions()
    {
        // specify the attribute code
        //$attributeCode = 'price';
        //
        //// get attribute model by attribute code, e.g. 'color'
        //$attributeModel = Mage::getSingleton('eav/config')
        //        ->getAttribute('catalog_product', $attributeCode);
        //
        //// build select to fetch used attribute value id's
        //$select = Mage::getSingleton('core/resource')
        //        ->getConnection('default_read')->select()
        //        ->from($attributeModel->getBackend()->getTable(), 'value')
        //        ->where('attribute_id=?', $attributeModel->getId());
        //        //->distinct();
        //SELECT * FROM `catalog_product_entity_datetime` where value > DATE_ADD(NOW(), INTERVAL 9 DAY)
        //// read used values from the db
        //$usedAttributeValues = Mage::getSingleton('core/resource')
        //        ->getConnection('default_read')
        //        ->fetchCol($select);
        
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        
        //select attribute id from attribute_code
        $select = $connection->select()
                        ->from('eav_attribute', array('attribute_id'))
                        ->where('attribute_code=?','expiry_date');
                        //->group('title');      
        //$sql        = "Select attribute_id from eav_attribute where attribute_code='expiry_date'";
        $rows       = $connection->fetchRow($select); //fetchRow($sql), fetchOne($sql),...
        $attr_id=$rows['attribute_id'];
        //end here
        
        //add ten days with current date
        $date = new Zend_Date(Mage::getModel('core/date')->timestamp());
        
        $select = $connection->select()
                        ->from('expiry_day', array('value'))
                        ->where('status=?','0');
                        //->group('title');      
        //$sql        = "Select attribute_id from eav_attribute where attribute_code='expiry_date'";
        $rows       = $connection->fetchRow($select); //fetchRow($sql), fetchOne($sql),...
        $day=$rows['value'];
        
        $date->addDay($day);
        $date->toString('Y-M-d H:m:s');
        $ten_days=$date->toString('Y-M-d 00:00:00');
        //end here
        
        //select entity id from attribute_id
        $select1 = $connection->select()
                        ->from('catalog_product_entity_datetime', array('entity_id'))
                        ->where('attribute_id=?',$attr_id)
                        ->where('value=?',$ten_days);
        $rows1       = $connection->fetchAll($select1); //fetchRow($sql), fetchOne($sql),...
        //end here
        
        
        $category = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('url_key', 'expiry-category');
        $cat_det=$category->getData();
        $category_id=$cat_det[0][entity_id];
        
        //$connection->beginTransaction();
        //foreach($rows1 as $prod_id)
        //{
        //    $__fields = array();
        //    $__fields['category_id'] = $category_id;
        //    $__where = $connection->quoteInto('product_id =?', $prod_id['entity_id']);
        //    $connection->update('catalog_category_product', $__fields, $__where);
        //     
        //    $connection->commit();
        //}
        return $category_id;
        //return $prod_id['entity_id'];
    }
}