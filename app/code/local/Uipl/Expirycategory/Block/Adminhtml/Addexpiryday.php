<?php


class Uipl_Expirycategory_Block_Adminhtml_Addexpiryday extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_addexpiryday";
	$this->_blockGroup = "expirycategory";
	$this->_headerText = Mage::helper("expirycategory")->__("Addexpiryday Manager");
	$this->_addButtonLabel = Mage::helper("expirycategory")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}