<?php
class Uipl_Expirycategory_Block_Adminhtml_Addexpiryday_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("addexpiryday_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("expirycategory")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("expirycategory")->__("Item Information"),
				"title" => Mage::helper("expirycategory")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("expirycategory/adminhtml_addexpiryday_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
