<?php
class Uipl_Expirycategory_Block_Adminhtml_Addexpiryday_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("expirycategory_form", array("legend"=>Mage::helper("expirycategory")->__("Item information")));

				
						$fieldset->addField("value", "text", array(
						"label" => Mage::helper("expirycategory")->__("Expiry Day"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "value",
						));
									
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('expirycategory')->__('Status'),
						'values'   => Uipl_Expirycategory_Block_Adminhtml_Addexpiryday_Grid::getValueArray1(),
						'name' => 'status',					
						"class" => "required-entry",
						"required" => true,
						));

				if (Mage::getSingleton("adminhtml/session")->getAddexpirydayData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getAddexpirydayData());
					Mage::getSingleton("adminhtml/session")->setAddexpirydayData(null);
				} 
				elseif(Mage::registry("addexpiryday_data")) {
				    $form->setValues(Mage::registry("addexpiryday_data")->getData());
				}
				return parent::_prepareForm();
		}
}
