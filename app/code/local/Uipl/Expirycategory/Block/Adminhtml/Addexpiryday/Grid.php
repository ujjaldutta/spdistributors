<?php

class Uipl_Expirycategory_Block_Adminhtml_Addexpiryday_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("addexpirydayGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("expirycategory/addexpiryday")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("expirycategory")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("value", array(
				"header" => Mage::helper("expirycategory")->__("Expiry Day"),
				"index" => "value",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('expirycategory')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Uipl_Expirycategory_Block_Adminhtml_Addexpiryday_Grid::getOptionArray1(),				
						));
						

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_addexpiryday', array(
					 'label'=> Mage::helper('expirycategory')->__('Remove Addexpiryday'),
					 'url'  => $this->getUrl('*/adminhtml_addexpiryday/massRemove'),
					 'confirm' => Mage::helper('expirycategory')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray1()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray1()
		{
            $data_array=array();
			foreach(Uipl_Expirycategory_Block_Adminhtml_Addexpiryday_Grid::getOptionArray1() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}