<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Uipl_Expirycategory_Model_Cron
{	
	public function changeCategory()
	{
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        
		//select attribute id from attribute_code
		$select = $connection->select()
				->from('eav_attribute', array('attribute_id'))
				->where('attribute_code=?','expiry_date');
				//->group('title');      
		//$sql        = "Select attribute_id from eav_attribute where attribute_code='expiry_date'";
		$rows       = $connection->fetchRow($select); //fetchRow($sql), fetchOne($sql),...
		$attr_id=$rows['attribute_id'];
		//end here
		
		//add ten days with current date
		$date = new Zend_Date(Mage::getModel('core/date')->timestamp());
		
		$select = $connection->select()
				->from('expiry_day', array('value'))
				->where('status=?','0');
				//->group('title');      
		//$sql        = "Select attribute_id from eav_attribute where attribute_code='expiry_date'";
		$rows       = $connection->fetchRow($select); //fetchRow($sql), fetchOne($sql),...
		$day=$rows['value'];
		
		$date->addDay($day);
		$date->toString('Y-M-d H:m:s');
		$ten_days=$date->toString('Y-M-d 00:00:00');
		//end here
		
		//select entity id from attribute_id
		$select1 = $connection->select()
				->from('catalog_product_entity_datetime', array('entity_id'))
				->where('attribute_id=?',$attr_id)
				->where('value=?',$ten_days);
		$rows1       = $connection->fetchAll($select1); //fetchRow($sql), fetchOne($sql),...
		//end here
		
		
		$category = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('url_key', 'expiry-category');
		$cat_det=$category->getData();
		$category_id=$cat_det[0][entity_id];
		
		$connection->beginTransaction();
		foreach($rows1 as $prod_id)
		{
		    $__fields = array();
		    $__fields['category_id'] = $category_id;
		    $__where = $connection->quoteInto('product_id =?', $prod_id['entity_id']);
		    $connection->update('catalog_category_product', $__fields, $__where);
		     
		    $connection->commit();
		}
		
		//$from_email = Mage::getStoreConfig('trans_email/ident_general/email'); //fetch sender email Admin
		//$from_name = Mage::getStoreConfig('trans_email/ident_general/name'); //fetch sender name Admin
		
		$userArray = Mage::getSingleton('admin/session')->getData();

		$user = Mage::getSingleton('admin/session');
		$userId = $user->getUser()->getUserId();
		$userEmail = $user->getUser()->getEmail();
		//$userFirstname = $user->getUser()->getFirstname();
		//$userLastname = $user->getUser()->getLastname();
		//$userUsername = $user->getUser()->getUsername();
		//$userPassword = $user->getUser()->getPassword();
		
		$body = "Some product has expired.<br>Please login to admin and see the list.";
		$mail = Mage::getModel('core/email');
		$mail->setToName($userId);
		$mail->setToEmail($userEmail);
		$mail->setBody($body);
		$mail->setSubject('Product expired alert');
		$mail->setFromEmail($userEmail);
		$mail->setFromName("Admin");
		$mail->setType('html');// You can use 'html' or 'text'
		
		try {
		    $mail->send();
		   // Mage::getSingleton('core/session')->addSuccess('Your mail has been sent');
		    return 'Your mail has been sent';
		}
		catch (Exception $e) {
		    //Mage::getSingleton('core/session')->addError('Unable to send.');
		    return 'Unable to send.';
		}
	} 
}