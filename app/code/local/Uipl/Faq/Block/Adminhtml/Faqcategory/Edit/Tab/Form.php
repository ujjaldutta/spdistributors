<?php
class Uipl_Faq_Block_Adminhtml_Faqcategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("faq_form", array("legend"=>Mage::helper("faq")->__("Item information")));

				
						$fieldset->addField("cat_name", "text", array(
						"label" => Mage::helper("faq")->__("Category"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "cat_name",
						));
									
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('faq')->__('Status'),
						'values'   => Uipl_Faq_Block_Adminhtml_Faqcategory_Grid::getValueArray2(),
						'name' => 'status',					
						"class" => "required-entry",
						"required" => true,
						));
						 
						//$fieldset->addField('post_date', 'select', array(
						//'label'     => Mage::helper('faq')->__('Post Date'),
						//
						//'values'   => Uipl_Faq_Block_Adminhtml_Faqcategory_Grid::getValueArray22(),
						//'name' => 'post_date',
						//));

				if (Mage::getSingleton("adminhtml/session")->getFaqcategoryData())
				{
					//$form->setValues(Mage::getSingleton("adminhtml/session")->getFaqcategoryData());
					Mage::getSingleton("adminhtml/session")->setFaqcategoryData(null);
				} 
				elseif(Mage::registry("faqcategory_data")) {
				    $form->setValues(Mage::registry("faqcategory_data")->getData());
				}
				return parent::_prepareForm();
		}
}
