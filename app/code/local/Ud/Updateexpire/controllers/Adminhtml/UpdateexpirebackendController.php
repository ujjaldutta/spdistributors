<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Ud_Updateexpire_Adminhtml_UpdateexpirebackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Batch Update of Product"));
	   $this->renderLayout();
    }
    
    public function updateAction(){
	
	 $post=$this->getRequest()->getPost();
        
        if(isset($post['submit'])){
	if($post['myform']['sku']=='' && $post['myform']['product_batch']==''){
		//print_r($post['myform']);exit;
		 $message = $this->__('Please enter valid product identity.');
           Mage::getSingleton('core/session')->addError($message);                    
                                 
	   $this->_redirect('*/*/index');
	   return;
	}
	$model = Mage::getSingleton('catalog/product_action')
        ->getResource();

		$collection = Mage::getModel('catalog/product')->getCollection()
			->setStoreId(1);
			if($post['myform']['sku']!=''){
			$collection->addAttributeToFilter('sku', array('eq' => $post['myform']['sku']));
			}elseif($post['myform']['product_batch']!=''){
				
			$collection->addAttributeToFilter('product_batch', array('eq' => $post['myform']['product_batch']));	
			}
		//echo $collection->getSelect()->__toString();exit;
		if($collection->count()<=0){
			 $message = $this->__('No product found for update');
			Mage::getSingleton('core/session')->addError($message);                    
					      
			$this->_redirect('*/*/index');
			return;
		}
		foreach ($collection as $product) {
		 $_product = Mage::getModel('catalog/product')->load($product->getId());
		    $_product->setStoreId(1)->setExpiryBymonth(array(0));
		    $_product->setStockData(
					array(
					   'manage_stock' => 1,
					   'use_config_manage_stock' => 0,	
					   'is_in_stock' => 1,
					   'qty' => $post['myform']['qty']
					)
				   );
		    $_product->setStatus(1);
		  $categories_pd= $_product->getCategoryIds();
		   
		  $key = array_search('5', $categories_pd);
		  unset($categories_pd[$key]);
		
		  $_product->setStoreId(1)->setCategoryIds($categories_pd);
		    $_product->setExpiryDate($post['myform']['expiry_date']);
		    $_product->save();
		   
		}
		       
			
	   $message = $this->__('Product is successfully updated. Please check the Manage Products option.');
           Mage::getSingleton('core/session')->addSuccess($message);                    
                                 
	   $this->_redirect('*/*/index');

	}
    }
}