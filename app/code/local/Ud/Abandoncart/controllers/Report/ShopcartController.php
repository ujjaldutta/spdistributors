<?php
require_once("Mage/Adminhtml/controllers/Report/ShopcartController.php");
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ud_Abandoncart_Report_ShopcartController extends Mage_Adminhtml_Report_ShopcartController
{
      public function _initAction()
    {
        $act = $this->getRequest()->getActionName();
        $this->loadLayout()
            ->_addBreadcrumb(Mage::helper('reports')->__('Reports'), Mage::helper('reports')->__('Reports'))
            ->_addBreadcrumb(Mage::helper('reports')->__('Shopping Cart'), Mage::helper('reports')->__('Shopping Cart'));
        return $this;
    }
    public function sendemailAction()
    {
       $this->_title($this->__('Reports'))
             ->_title($this->__('Shopping Cart'))
             ->_title($this->__('Abandoned Carts'));

        $this->_initAction()->_setActiveMenu('report/shopcart/abandoned');
        $this->_addBreadcrumb(Mage::helper('reports')->__('Abandoned Carts'), Mage::helper('reports')->__('Abandoned Carts Send Email'));
        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
        
        $post=$this->getRequest()->getPost();
        
        if(isset($post['submit'])){
            
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read'); 
            $wconn = Mage::getSingleton('core/resource')->getConnection('core_write');
            $wconn->query("Delete from abandoned_mail where customer_id='".$post['customer_id']."'");
            $wconn->query("INSERT INTO 	abandoned_mail set customer_id='".$post['customer_id']."', sentdate=NOW(), sent='Y'");
            
            
                                 $emailTemplate  = Mage::getModel('core/email_template')
						->loadDefault('general_abandoncart_email');
                                 
                                 $customerData = Mage::getModel('customer/customer')->load($post['customer_id']);
                                  $storeid = Mage::app()->getStore()->getId();
				  $storeObj = Mage::getModel('core/store')->load($storeid);
                                 $email_template_variables = array(
                                 'customer' =>  $customerData,
                                'emailcontent'=>$post['myform']['contents'],
                                 'store'=>$storeObj
                                 );
                                 $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
                                 
                                 $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
                                 
                                 
                                 $sender = array('name' => $sender_name,
                                 'email' => $sender_email);
                                 $email_to=$post['myform']['email'];
                                 
                                 
                                
                                 $translate  = Mage::getSingleton('core/translate');
                          
                          
				$processedTemplate = $emailTemplate->getProcessedTemplate($email_template_variables);

				    $emailTemplate->setTemplateSubject($post['myform']['subject']);
				    
				    $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email', $store));
				    
				    $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name', $store));

                                
				
                                $emailTemplate->send($email_to,$customerData->getName(), $email_template_variables);
                                 
                                 
             $message = $this->__('Email is successfully sent to user.');
            Mage::getSingleton('core/session')->addSuccess($message);                    
                                 
          $this->_redirect('*/*/abandoned');
            
        }
  //echo get_class($this->getLayout()->createBlock('ud_abandoncart/adminhtml_sendmail'));exit;
       // $this->_addContent($this->getLayout()->createBlock('adminhtml/report_shopcart_abandoned_edit'));
        $this->_addContent($this->getLayout()->createBlock('ud_abandoncart/adminhtml_sendmail') ->setTemplate('abandoned/sendemail.phtml'));
       /* $this->_addContent($this->getLayout()->createBlock('form/adminhtml_form_edit'))
                ->_addLeft($this->getLayout()->createBlock('form/adminhtml_form_edit_tabs'));*/
        $this->renderLayout();
                                
    }
    
    
    
    
    
    
    
    //banner/adminhtml_banner_edit
    /* $this->_addContent($this->getLayout()->createBlock('adminhtml/report_shopcart_abandoned_edit'));
        
        
        $this->getLayout()
                ->createBlock('ud_abandoncart/adminhtml_sendmail')
                ->setTemplate('cart/test.phtml')
                ->renderLayout();
     * 
     * 
     */
 
}
