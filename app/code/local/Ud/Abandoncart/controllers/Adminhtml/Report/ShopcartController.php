<?php
require_once("Mage/Adminhtml/controllers/Report/ShopcartController.php");
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Shopping Cart reports admin controller
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Ud_Abandoncart_Adminhtml_Report_ShopcartController extends Mage_Adminhtml_Report_ShopcartController
{
      public function _initAction()
    {
        $act = $this->getRequest()->getActionName();
        $this->loadLayout()
            ->_addBreadcrumb(Mage::helper('reports')->__('Reports'), Mage::helper('reports')->__('Reports'))
            ->_addBreadcrumb(Mage::helper('reports')->__('Shopping Cart'), Mage::helper('reports')->__('Shopping Cart'));
        return $this;
    }
    public function sendemailAction()
    {
       $this->_title($this->__('Reports'))
             ->_title($this->__('Shopping Cart'))
             ->_title($this->__('Abandoned Carts'));

        $this->_initAction()->_setActiveMenu('report/shopcart/abandoned');
        $this->_addBreadcrumb(Mage::helper('reports')->__('Abandoned Carts'), Mage::helper('reports')->__('Abandoned Carts Send Email'));
        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
  
        //$this->_addContent($this->getLayout()->createBlock('adminhtml/report_shopcart_abandoned_edit'));
        $this->getLayout()
                ->createBlock('ud_abandoncart/adminhtml_sendmail')
                ->setTemplate('cart/test.phtml');
        $this->renderLayout();
                                
    }
    //banner/adminhtml_banner_edit
    /* $this->_addContent($this->getLayout()->createBlock('adminhtml/report_shopcart_abandoned_edit'));
        
        
        $this->getLayout()
                ->createBlock('ud_abandoncart/adminhtml_sendmail')
                ->setTemplate('cart/test.phtml')
                ->renderLayout();
     * 
     * 
     */
 
}
