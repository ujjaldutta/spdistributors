<?php
class Ud_Abandoncart_Block_Adminhtml_Report_Shopcart_Abandoned_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
                    
				parent::__construct();
				$this->setId("report_abandoned");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("reports")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
                   echo get_class($this->getLayout()->createBlock("adminhtml/report_shopcart_abandoned_edit_tab_form"));
				$this->addTab("form_section", array(
				"label" => Mage::helper("reports")->__("Item Information"),
				"title" => Mage::helper("reports")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("adminhtml/report_shopcart_abandoned_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
